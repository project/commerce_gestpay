

# Drupal Commerce Gestpay integration module


## Introduction

Commerce Gestpay module allows to accept payments using Axerve E-commerce
Solutions (formerly known as Gestpay by Banca Sella). Currently it supports
off-site payment, redirecting the user on Banca Sella&rsquo;s page to finish payment.


## Requirements

This module uses **PHP Soap** to initiate the communication with remote server.
Make sure to install and enable **PHP Soap** library on your server.


## Usage

First you have to setup the module on Drupal Commerce:

1.  Install the module.
2.  Enable the module.
3.  Add the payment gateway from `/admin/commerce/config/payment-gateways`.
4.  Configure the payment gateway:
    -   Gateway name. The *machine name* will be used to configure Gestpay Backoffice
        settings, put as `{payment_gateway_id}` in Gestpay URLs settings (see below).
    -   Plugin: **Gestpay (redirect to Gestpay)**.
    -   Mode: select **Test** if you want to use the Gestpay sandbox, select **Live** otherwise.
    -   Shop login: put your Gestpay ID (something like `GESPAY1234`).
    -   Select the currency code.

When you&rsquo;ve create the payment gateway you can configure the settings on
Gestpay Backoffice:

-   Environment settings:
    -   MOTO: **Unify transaction**
    -   URL for positive response:
        `https://example.com/payment_commerce_gestpay/continue/{payment_gateway_id}`
    -   URL for negative response:
        `https://example.com/payment_commerce_gestpay/error/{payment_gateway_id}`
    -   URL Server to server:
        `https://example.com/payment_commerce_gestpay/capture/{payment_gateway_id}`
-   Security:
    -   Security Level: **IP**
    -   IP Address: your server IP address


## Roadmap

-   Add *APIKEY* security level.
-   Add on-site payment support.


## Credits and sponsor

This module has been developed by [Leonardo Finetti](https://www.siti-drupal.it) thanks to [Digital km zero
Srl](http://www.marketingkmzero.it/) sponsorship and support.

