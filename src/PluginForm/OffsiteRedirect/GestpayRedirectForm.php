<?php

namespace Drupal\commerce_gestpay\PluginForm\OffsiteRedirect;

use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_gestpay\GestpaySoap;

class GestpayRedirectForm extends PaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Load payment entity
    $payment = $this->entity;

    // Load payment gateway plugin
    $plugin = $payment->getPaymentGateway()->getPlugin();

    // So we can gat payment gateway settings
    $configuration = $plugin->getConfiguration();

    // Shop login from payment plugin settings
    $shop_login = $configuration['shop_login'];

    // Uic code (currency) from payment plugin settings
    $uic_code = $configuration['uic_code'];

    // Load order ID from current uri
    $order_id = \Drupal::routeMatch()->getParameter('commerce_order')->id();

    // Order total amount
    $amount = $payment->getAmount()->getNumber();

    // Set Gestpay parameters
    $params = new \stdClass();
    $params->shopLogin = $shop_login;
    $params->uicCode = $uic_code;
    $params->amount = round(floatval($amount), 2);

    $params->shopTransactionId = $order_id;

    // @TODO: Api Key authentication method:
    // $params->apikey = '';

    // @TODO: Optional parameter for on-site payment. Currently not implemented.
    // $params->buyerName = '';
    // $params->buyerEmail = '';
    // $params->languageId = '';
    // $params->customInfo = '';
    // $params->cardNumber = '';
    // $params->expiryMonth = '';
    // $params->expiryYear = '';
    // $params->cvv = '';

    // Create SOAP client for encrypt and decrypt data using payment gateway id
    $gestpay_soap_client = new GestpaySoap($payment->getPaymentGateway()->id());

    // Encrypt the request with the current order parameters
    $xml = $gestpay_soap_client->getEncryptResponse($params);

    // Check WSCryptDecrypt result
    if ($xml->TransactionResult == "OK" ) { // If OK

      // Create the redirect link
      $link = $gestpay_soap_client->getGestpayLink($shop_login, $xml->CryptDecryptString);

      // Create the redirect to off-site payment page
      return $this->buildRedirectForm($form, $form_state, $link, [], self::REDIRECT_GET);
    } else { // KO: transaction error; XX: suspended transaction (only with bank transfer)

      // Log error to Drupal log
      \Drupal::logger('commerce_gestpay')->error('Error <code>'. print_r($xml->ErrorCode, true) . '</code> - ' . print_r($xml->ErrorDescription, true) . '</pre>');

      // Print error message on screen
      \Drupal::messenger()->addMessage('Error: ' . $xml->ErrorCode . ' - ' . $xml->ErrorDescription, 'error');

      // Redirect to previous step ('order_information')
      // @TODO: improve the step selection instead of hardcoding 'order_information'.
      throw new NeedsRedirectException(Url::fromRoute('commerce_checkout.form', [
        'commerce_order' => $order_id,
        'step' => 'order_information',
      ])->toString());
    }
  }

}
