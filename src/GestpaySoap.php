<?php

namespace Drupal\commerce_gestpay;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Url;
use Drupal\Core\Entity\Entity;
use Drupal\Core\Controller\ControllerBase;
use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\commerce_order\Entity\Order;

use SoapClient;

class GestpaySoap {

  /**
   * The SOAP client used for encrypt/decrypt data.
   */
  protected $client;

  /**
   * The payment server (test/live).
   */
  protected $server_mode;

  /**
   * Constructs a new GestpaySoap connection object for WsCryptDecrypt API.
   */
  public function __construct($payment_name) {

    // Load payment gateway from payment name
    $payment_gateway = PaymentGateway::load($payment_name);

    // get payment plugin
    /** @var \Drupal\commerce_gestpay\Plugin\Commerce\PaymentGateway\GestpayRedirect $plugin */
    $plugin = $payment_gateway->getPlugin();

    // Set server mode settings from payment plugin settings
    $this->server_mode = $plugin->getMode() === 'test' ? 'test' : 'live';

    // Call Gestpay WSCryptDecrypt WebService using SOAP and save on protected
    // variable
    $this->client = new SoapClient($this->commerce_gestpay_wscryptdecrypt_url($this->server_mode));
  }

  /**
   * Encrypt data using Gestpay SOAP webservice and return XML response.
   *
   * Used on GestpayRedirectForm to create the encrypted string for the offsite
   * redirect link parameter.
   *
   * @param stdClass $params
   *   The params object to encrypt
   *
   * @return
   *   XML response from Gestpay SOAP webservice
   */
  public function getEncryptResponse($params){

    // Load the current SOAP client
    $client = $this->client;

    // Encrypt data
    $objectresult = $client->Encrypt($params);

    // Get encrypted data result from API
    $simpleresult = $objectresult->EncryptResult;

    // Load XML result
    $xml = simplexml_load_string($simpleresult->any);

    // Return XML data
    return $xml;
  }

  /**
   * Decrypt data using Gestpay SOAP webservice and return XML response.
   *
   * Used on CommerceGestpayNotificationController for decryping the data from
   * Gestpay.
   *
   * @param stdClass $params
   *   The params object to decrypt
   *
   * @return
   *   XML response from Gestpay SOAP webservice
   */
  public function getDecryptResponse($params){
    // Load the current SOAP client
    $client = $this->client;

    // Decrypt data
    $objectresult = $client->Decrypt($params);

    // Get decrypted data result from API
    $simpleresult = $objectresult->DecryptResult;

    // Load XML result
    $xml = simplexml_load_string($simpleresult->any);

    // Return XML data
    return $xml;
  }

  /**
   * Return the Gestpay redirect link using the current server mode.
   *
   * @param string $a
   *   The shop login
   * @param string $b
   *   The encrypted string
   */
  public function getGestpayLink($a, $b){
    // Set the base URL to the specified Gestpay server
    switch ($this->server_mode) {
      case 'test':
        $gestpay_url = 'https://testecomm.sella.it/pagam/pagam.aspx';
        break;
      case 'live':
        $gestpay_url = 'https://ecomm.sella.it/pagam/pagam.aspx';
        break;
    }

    // Compose the URL with the parameters
    return  $gestpay_url . "?a=" . $a . "&b=" . $b;
  }

  /**
   * Returns the URL with the WSCryptDecrypt webservice.
   *
   * @return string
   *   The URL of the WSCryptDecrypt webservice
   */
  private function commerce_gestpay_wscryptdecrypt_url() {
    switch ($this->server_mode) {
      case 'test':
        return 'https://sandbox.gestpay.net/gestpay/gestpayws/WSCryptDecrypt.asmx?WSDL';
        break;
      case 'live':
        return 'https://ecommS2S.sella.it/gestpay/gestpayws/WSCryptDecrypt.asmx?WSDL';
        break;
    }
  }

}
