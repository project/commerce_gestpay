<?php

namespace Drupal\commerce_gestpay\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Url;
use Drupal\Core\Entity\Entity;
use Drupal\Core\Controller\ControllerBase;
use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_gestpay\GestpaySoap;


/**
 * Commerce Gestpay route controller used for manage server to server endpoint
 * and positive/negative URLs.
 */
class CommerceGestpayNotificationController extends ControllerBase {

  /**
   * Server to server: handle the Gestpay IPN for succesful payment.
   * This method allows the payment to be processed even if the user does not
   * return to the site after paying off-site.
   *
   * @param string $payment_name
   *   The machine name for the current payment from URL address.
   */
  public function paymentCapture($payment_name) {
    // Get parameters from URL
    $params = $this->geturlparameters();

    // Load payment gateway from payment name. The payment name need to be set
    // on Gestpay backoffice settings.

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = PaymentGateway::load($payment_name);

    // If the gateway exists, then proceed with the payment. Otherwise return
    // an error.
    if ($payment_gateway){

      // Create SOAP client for encrypt/decrypt data using the payment plugin
      $gestpay_soap_client = new GestpaySoap($payment_name);

      // Decrypt the request with the current URL parameters
      $xml = $gestpay_soap_client->getDecryptResponse($params);

      // Load data from XML
      $xml_result = (string)$xml->TransactionResult;
      $xml_order_id = (string)$xml->ShopTransactionID;
      $xml_remote_id = (string)$xml->BankTransactionID;
      $xml_amount = (string)$xml->Amount;
      $xml_currency = (string)$xml->Currency;

      // If payment is OK, then save the payment
      if($xml_result == 'OK'){

        // Load the order
        $order = \Drupal\commerce_order\Entity\Order::load($xml_order_id);

        // Create the payment for the order. Setting 'completed' allows Commerce
        // to save the new order state (from "draft" to the next state according
        // with the current workflow).
        $payment = Payment::create([
          'type' => 'payment_default',
          'state' => 'completed',
          'amount' => $order->getTotalPrice(),
          'payment_gateway' => $payment_name,
          'order_id' => $order->id(),
          'remote_id' => $xml_remote_id,
          'remote_state' => 'completed',
        ]);

        // Save the payment
        $payment->save();

        // Set response message
        $result = 'OK';
      } else {
        // Theoretically gestpay should not make the "server to server" call if
        // the payment is not successful.
        $result = 'KO';

        // Log error to Drupal log
        \Drupal::logger('commerce_gestpay')->error('Payment error (server to server request): <pre>'.print_r($xml, true) . '</pre>');

        // @TODO: should we throw an error like the following one?
        //        throw new PaymentGatewayException('', '');
      }
    } else {
      // If the payment gateway does not exist, is it perhaps the case to log
      // the error?

      // This is an extreme case as it can only occur in two series:
      // 1. The Gestpay configuration has an incorrect payment ID
      // 2. The configuration was correct but the payment gateway was deleted
      //    from the site.
      // In both cases it is a configuration problem, not a module problem.
      $result = 'KO';

      // Log error to Drupal log
      \Drupal::logger('commerce_gestpay')->error('Payment error (server to server request): <pre>'.print_r($xml, true) . '</pre>');

      // @TODO: should we throw an error like the following one?
      //        throw new PaymentGatewayException('', '');

    }

    // @TODO: send HTTP response?
    $response = new Response();
    $response->setContent($result);
    return $response;
  }

  /**
   * Positive response URL: get the Gestpay data when the user go back to the site.
   *
   * @param string $payment_name
   *   The machine name for the current payment from URL address.
   */
  public function paymentContinue($payment_name) {
    // Get parameters from URL
    $params = $this->geturlparameters();

    // Load payment gateway from payment name. The payment name need to be set
    // on Gestpay backoffice settings.

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = PaymentGateway::load($payment_name);

    // If the gateway exists, then proceed with the payment. Otherwise return
    // an error.
    if ($payment_gateway){

      // Create SOAP client for encrypt/decrypt data using the payment plugin
      $gestpay_soap_client = new GestpaySoap($payment_name);

      // Decrypt the request with the current URL parameters
      $xml = $gestpay_soap_client->getDecryptResponse($params);

      // Check if the transaction is OK
      if($xml->TransactionResult == 'OK'){

        // Load the order
        $order = \Drupal\commerce_order\Entity\Order::load((string)$xml->ShopTransactionID);

        // Redirect to last step ('complete')
        // @TODO: improve the step selection instead of hardcoding 'complete'.
        throw new NeedsRedirectException(Url::fromRoute('commerce_checkout.form', [
          'commerce_order' => $order->id(),
          'step' => 'complete',
        ])->toString());

      } else {

        // NOTICE: This case should never trigger because the "positive" url is
        // called only if the payment is OK.

        // Log error to Drupal log
        \Drupal::logger('commerce_gestpay')->error('Payment error: <pre>'.print_r($xml, true) . '</pre>');

        // Prints error if payment gateway is missing:
        $build = [
          '#markup' => $this->t('Something went wrong!'),
        ];
        return $build;

      }

    } else {
      // Prints error if payment gateway is missing:
      $build = [
        '#markup' => $this->t('Missing payment gateway (@payment_name), check your confing!', ['@payment_name' => $payment_name]),
      ];
      return $build;
    }
  }

  /**
   * Returns a render-able array for a test page.
   */
  public function paymentError($payment_name) {
    // Get parameters from URL
    $params = $this->geturlparameters();

    // Load payment gateway from payment name. The payment name need to be set
    // on Gestpay backoffice settings.

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = PaymentGateway::load($payment_name);

    // If the gateway exists, then proceed with the payment. Otherwise return
    // an error.
    if ($payment_gateway){

      // Create SOAP client for encrypt/decrypt data using the payment plugin
      $gestpay_soap_client = new GestpaySoap($payment_name);

      // Decrypt the request with the current URL parameters
      $xml = $gestpay_soap_client->getDecryptResponse($params);

      // Log error to Drupal log
      \Drupal::logger('commerce_gestpay')->error('Payment error: <pre>'.print_r($xml, true) . '</pre>');

      // Print error message on screen
      \Drupal::messenger()->addMessage($this->t('Error @error_code: @error_description', ['@error_code' => (string)$xml->ErrorCode, '@error_description' => (string)$xml->ErrorDescription]), 'error');

      // Load the order
      $order = \Drupal\commerce_order\Entity\Order::load((string)$xml->ShopTransactionID);

      // Unlock the order
      $order->unlock();

      // Save the unlocked order
      $order->save();

      // Redirect to previous step ('order_information')
      // @TODO: improve the step selection instead of hardcoding 'order_information'.
      throw new NeedsRedirectException(Url::fromRoute('commerce_checkout.form', [
        'commerce_order' => $order->id(),
        'step' => 'order_information',
      ])->toString());
    } else {
      // Prints error if payment gateway is missing
      $build = [
        '#markup' => $this->t('Missing payment gateway (@payment_name), check your confing!', ['@payment_name' => $payment_name]),
      ];
      return $build;
    }
  }

  /**
   * Returns the custom params object.
   *
   * @return
   *   A standard PHP object with the parameters from URL
   */
  private function getUrlParameters() {
    // Get parameters from URL
    $params = new \stdClass();
    $params->shopLogin = $_GET["a"];
    $params->CryptedString = $_GET["b"];

    // Return the object
    return $params;
  }

}
