<?php

namespace Drupal\commerce_gestpay\Plugin\Commerce\PaymentGateway;

use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;

/**
 * Provides Gestpay offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "gestpay_redirect_checkout",
 *   label = @Translation("Gestpay (redirect to Gestpay)"),
 *   display_label = @Translation("Gestpay"),
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_gestpay\PluginForm\OffsiteRedirect\GestpayRedirectForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "maestro", "mastercard", "visa",
 *   },
 * )
 */
class GestpayRedirect extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {

    return [
      'shop_login' => '',
      'uic_code' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['shop_login'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Shop login'),
      '#description' => $this->t('Login assigned by Gestpay backend.'),
      '#default_value' => $this->configuration['shop_login'],
      '#required' => TRUE,
    ];

    $form['uic_code'] = [
      '#type' => 'select',
      '#title' => $this->t('Currency code'),
      '#description' => $this->t('Transactions can only be processed in one of the listed currencies'),
      '#default_value' => $this->configuration['uic_code'],
      '#required' => TRUE,
      '#options' => [
        '242' => $this->t('Euro'),
        '1' =>   $this->t('US dollar'),
        '2' =>   $this->t('GB pound'),
        '3' =>   $this->t('Swiss franc'),
        '7' =>   $this->t('Danish krone'),
        '8' =>   $this->t('Norwegian krone'),
        '9' =>   $this->t('Swedish krona'),
        '12' =>  $this->t('Canadian dollar'),
        '71' =>  $this->t('Japanese yen'),
        '103' => $this->t('Hong Kong dollar'),
        '234' => $this->t('Brazilian real'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['shop_login'] = $values['shop_login'];
      $this->configuration['uic_code'] = $values['uic_code'];
    }
  }

}
